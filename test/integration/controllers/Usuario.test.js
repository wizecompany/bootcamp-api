var expect = require('chai').expect;
var request = require('supertest');

describe('controllers:Usuario', function () {
  var loginAgent = null;

  describe('#login()', function () {
    it('deve permitir que um usuário realize login', function (done) {
      var loginData = {
        email: 'meu@email.com.br',
        senha: 'eusouumusuario'
      };

      loginAgent = request.agent(sails.hooks.http.app);
      loginAgent.post('/usuario/login')
        .send(loginData)
        .then(function (response) {
          expect(response.statusCode).to.equal(200);
          var responseBody = response.body;
          var dateToday = new Date();

          expect(responseBody).to.not.be.null;
          expect(responseBody.user).to.not.be.null;
          expect(responseBody.token).to.not.be.null;

          loggedToken = responseBody.user.tokenLogin;
          expect(loggedToken).to.be.equal(responseBody.token);

          var dateExpiration = new Date(responseBody.user.tokenExpires);
          expect(dateExpiration).to.be.above(dateToday);
          done();
        }).catch(done);
    });

    it('não deve ser possível que um usuário inexistente faça login', function (done) {
      var loginData = {
        email: 'usuario@dementira.com.br',
        senha: 'eunaoexisto'
      };

      var notLogged = request.agent(sails.hooks.http.app);
      notLogged.post('/usuario/login')
        .send(loginData)
        .then(function (response) {
          expect(response.statusCode).to.equal(401);
        }).catch(done);

      notLogged.get('/usuario/logout')
        .then(function (response) {
          expect(response.statusCode).to.equal(403);
          done();
        }).catch(done);
    });

    it('não deve ser possível fazer login com informações incompletas', function (done) {
      var loginData = {
        email: 'meu@email.com.br'
      };

      var notLogged = request.agent(sails.hooks.http.app);
      notLogged.post('/usuario/login')
        .send(loginData)
        .then(function (response) {
          expect(response.statusCode).to.equal(400);
        }).catch(done);

      notLogged.get('/usuario/logout')
        .then(function (response) {
          expect(response.statusCode).to.equal(403);
          done();
        }).catch(done);
    });
  });

  describe('#logout()', function () {
    it('usuários logados devem poder fazer logout', function (done) {
      loginAgent.get('/usuario/logout')
        .then(function (response) {
          expect(response.statusCode).to.equal(200);
          done();
        }).catch(done);
    });
  });
});
