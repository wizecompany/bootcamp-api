var expect = require('chai').expect;
var request = require('supertest');

var loggedUsuario = null;
before(function (done) {
  Usuario.find().limit(1).then(function (usuario) {
    usuario = usuario[0];
    loggedUsuario = request.agent(sails.hooks.http.app);
    return loggedUsuario.post('/usuario/login').send({ email: usuario.email, senha: usuario.senha });
  }).then(function (response) {
    expect(response.statusCode).to.equal(200);
    done();
  }).catch(done);
});

describe('models:Usuario', function () {
  describe('#find()', function () {
    it('deve retornar usuários cadastrados', function (done) {
      loggedUsuario.get('/usuario')
        .then(function (response) {
          expect(response.statusCode).to.equal(200);
          expect(response.body).to.be.an('array');
          expect(response.body).to.not.be.empty;
          done();
        }).catch(done);
    });
  });
});
