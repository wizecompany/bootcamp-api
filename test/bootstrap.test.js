var sails = require('sails');

var Barrels = require('barrels');
var barrels = new Barrels();

before(function (done) {
  this.timeout(5000);

  sails.lift({
  }, function (err) {
    if (err) return done(err);

    barrels.populate([
      'usuario',
      'contato'
    ], function (err) {
      done(err, sails);
    });
  });
});

after(function (done) {
  sails.lower(done);
});
