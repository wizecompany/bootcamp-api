/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */

module.exports = function (req, res, next) {
  if (req.session.authenticated) {
    return next();
  }

  var token = req.headers.authorization ? req.headers.authorization : '';
  token = token.replace('Bearer ', '');

  AuthService.isValidToken(token, function (user) {
    if (user) {
      req.session.user = user;
      return next();
    } else {
      return res.forbidden('Você não possui permissão para realizar essa ação.');
    }
  });
};
