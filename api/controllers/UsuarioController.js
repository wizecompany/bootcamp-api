/**
 * UsuarioController
 *
 * @description :: Server-side logic for managing Usuarios
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  login: function (req, res) {
    var email = req.param('email');
    var senha = req.param('senha');

    setTimeout(function () {
      if (!email || !senha) return res.badRequest('Email e senha são obrigatórios.');
      AuthService.login(req.session, email, senha).then(function (usuarioData) {
        if (usuarioData) return res.ok(usuarioData);
        return res.unauthorized();
      }).catch(res.negotiate);
    }, 200);
  },

  logout: function (req, res) {
    AuthService.logout(req.session).then(res.ok).catch(res.negotiate);
  }
};
