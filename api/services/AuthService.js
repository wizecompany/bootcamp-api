const crypto = require('crypto');
const EXPIRE_TIMES = {
  test: 10,
  development: 1440,
  production: 360
};

module.exports = {
  isValidToken: function (token, callback) {
    return Usuario.findOne({
      tokenLogin: token,
      tokenExpires: { '>=': new Date() }
    }).then(callback).catch(callback);
  },

  login: function (session, email, senha) {
    return new Promise(async (resolve, reject) => {
      let userFound = null;
      try {
        userFound = await Usuario.findOne({
          email: email,
          senha: senha
        });
      } catch (error) {
        return reject(error);
      }

      if (!userFound) return resolve(null);

      let expires = new Date();
      expires.setMinutes(expires.getMinutes() + EXPIRE_TIMES[sails.config.environment]);

      Usuario.update({ id: userFound.id }, {
        tokenLogin: crypto.randomBytes(10).toString('hex'),
        tokenExpires: expires,
        lastLogin: new Date().toISOString()
      }).then(userUpdated => {
        userUpdated = userUpdated[0];
        session.authenticated = true;
        session.user = userUpdated;
        return resolve({ user: userUpdated, token: userUpdated.tokenLogin });
      }).catch(reject);
    });
  },

  logout: function (session) {
    return new Promise((resolve, reject) => {
      try {
        session.authenticated = false;
        session.user = null;
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  }
};
