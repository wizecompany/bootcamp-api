/**
 * Contato.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    // Primitivos
    email: { type: 'string' },
    empresa: { type: 'string' },
    idade: { type: 'integer' },
    foto: { type: 'string' },
    nome: { type: 'string', required: true },
    socialMedia: { type: 'string' },
    telefone: { type: 'string' },

    // Associações
    usuario: { model: 'usuario' }
  }
};
