/**
 * Usuario.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    // Primitivos
    email: { type: 'string', email: true, required: true, unique: true },
    senha: { type: 'string', required: true },
    tokenExpires: { type: 'datetime' },
    tokenLogin: { type: 'string' },

    // Associações
    contatos: { collection: 'contato', via: 'usuario' }
  }
};
