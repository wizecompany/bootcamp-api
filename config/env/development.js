/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the development       *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  // models: {
  //   connection: 'someMongodbServer'
  // }

  connections: {
    database: {
      adapter: 'sails-mongo',
      url: 'mongodb://heroku_ts113cb3:lihumdt1p6bbg7om287co6h6r2@ds259119.mlab.com:59119/heroku_ts113cb3'
    }
  }
};
